#!/bin/bash

##################
### TO DO:
## Add posibility to write to a different file with your name of choice.
## Add posibility to save as md file
##############

###This Function calculates the weight of the arrow and the FOC if balance point is provided##
clear
echo "######################################################################################################"
echo "Calculates arrow weight, grains per pound, FOC and Kinetic energy based on provided Arrow data."
echo "Please use dot (.) as comma separator."
echo "Default values are within parentesis. Please change to your own arrow and bow measures."
echo ""
echo "########################################################"
echo  Do you want to calculate:
echo "FOC - 1"
echo "ARROW WEIGHT and GRAINS PER POUND - 2"
echo "MOMENTUM and KINETIC ENERGY - 3"
echo "ALL of the above - 4"
echo ""
echo "##########################################################"
echo ""
read -p "Chose 1-4: " CALC

CALC=${CALC:=4}
clear
echo ""

if [[ $CALC -eq 1 || $CALC -eq 4 ]]; then
		echo -e "ARROW LENGTH is used to calculate FOC. AMO length is from nock grove to shaft end, but it can include nock and point if you wish. \n"
		read -p "ARROW LENGTH Inches : " Inch
		read -p "BALANCE POINT inches from nock (Optional, if not set weights is used to claculate FOC): " BalanceP
		echo ""
fi

if [[ $CALC -ge 2 || -z $BalanceP ]]; then
		
		read -p "SHAFT LENGTH (excluding nock and point): " SInch
		read -p "GRAINS PER INCH (Obligatory): " GrainPIn
		read -p "NR OF VANES (4): " Vanes
		read -p "VANE WEIGHT (2.3 Grains): " VaneW
		read -p "NOCK WEIGHT (12.2 Gr): " NockW
		read -p "POINT WEIGHT (125 Gr): " PointW
		read -p "INSERT WEIGHT (12.1 Gr): " InsertW
		read -p "EXTRA WEIGHTS (0 Gr): " ExtraW
		read -p "DRAW WEIGHT at your draw lenght (40 lb): " BowLB
fi

if [[ $CALC -ge 3 ]]; then
	echo ""
	read -p "ARROW SPEED in feet per second: " ASpeed
fi
echo ""

read -p "Do you want to SAVE THE DATA in a file (y/N)?: " ToFile


## Set some default values to variables
Inch=${Inch:=0}
SInch=${SInch:=0}
BowLB=${BowLB:=40}

Vanes=${Vanes:=4}
VaneW=${VaneW:=2.3}
NockW=${NockW:=12.2}
PointW=${PointW:=125}
InsertW=${InsertW:=12.1}
ExtraW=${ExtraW:=0}

ASpeed=${ASpeed:=0}

Swe=${Swe:="not chosen to be calculated."}


echo ""
echo ""

## Make some test to assure necessary variables are set.
# For FOC
if [[ $CALC -eq 1 && $Inch == 0 || $CALC -eq 4 && $Inch == 0 ]]; then
     echo "You must provide a value for Arrow length"
     read -p "Arrow length in inches: " Inch

		if [[ -z $Inch ]]; then
				clear
		     echo ""
		     echo ""
		     echo "YOU DID NOT PROVIDE A ARROW LENGTH."
		     echo "WILL NOW EXIT!"
				 sleep 1
				 exit
		fi
fi


## For Weight measures.

if [[ $CALC -ge 2 && $SInch == 0 ]]; then
     echo "You must provide a value for Shaft length"
     read -p "Shaft length in inches: " SInch

		if [[ -z $SInch ]]; then
				clear
		     echo ""
		     echo ""
		     echo "YOU DID NOT PROVIDE A SHAFT LENGTH."
		     echo "WILL NOW EXIT!"
				 sleep 1
				 exit
		fi
fi

if [[ $CALC -ge 2 && -z $GrainPIn ]]; then
    echo "Without a value for Grains per inch only FOC can be calculated."
    read -p "Grains per Inch: " GrainPIn
		KE=${KE:="Not posible to calculate without arrow weight data."}
		MM=${MM:="Not posible to calculate without arrow weight data."}
fi

## If Grains per Inch yet is not set, give them a value of 0 to make the other measures anyway.
#GrainPIn=${GrainPIn:=0}

clear
echo ""


case $CALC in
		1)
        Bal=$( echo "scale=3;($BalanceP - ($Inch/2)) /$Inch * 100" | bc) ;;  # FOC

		2|3|4)
				Swe=$( echo "scale=3;($GrainPIn * $SInch) + ($Vanes * $VaneW) + ($NockW + $PointW + $InsertW + $ExtraW)" | bc) #Arrow weight
				Agr=$( echo "scale=3;$Swe / $BowLB" | bc)

		
				if [[ $CALC -gt 3 && -z $BalanceP ]]; then
						## Calculating Balancepoint.
						## Weihts
						CW=$( echo "scale=3; $GrainPIn * $SInch" | bc)
				  	EW=$( echo "scale=3; ($Vanes * $VaneW) + $NockW" | bc)
						PW=$( echo "scale=3; $PointW + $InsertW + $ExtraW" |bc)
				  	## Distances
						CD=$( echo "scale=3; $Inch / 2" | bc)
						ED=1.2
						PD=$( echo "scale=3; $Inch - 1" | bc)
						## Calculate sums of momentum and weights
						SM=$( echo "scale=3; ($CW * $CD) + ($EW * $ED) + ($PW * $PD)" | bc)
						SW=$( echo "scale=3; $CW + $EW + $PW" | bc)
						# Calculate COG
						BalanceP=$( echo "scale=3; $SM / $SW" | bc)
						BalanceP=$( echo "scale=2; ($BalanceP * 0.98479841375)/1" | bc)			# Correction for balance point calc error. The division by 1 because the scale is only effective in division.
						#Calculate FOC
				  	Bal=$( echo "scale=2;($BalanceP - ($Inch/2)) /$Inch * 100" | bc)  # FOC
						echo ""
				
				elif [[ $CALC -gt 3 ]]; then
						Bal=$( echo "scale=3;($BalanceP - ($Inch/2)) /$Inch * 100" | bc)  # FOC
				else
						Bal=${Bal:=""not chosen to be calculated.}
				fi
					
				if [[ $ASpeed != 0 ]]; then
						KEa=$( echo "scale=3;($Swe * $ASpeed * $ASpeed) / 450240" | bc) #Kinetic energy
						KE="${KEa} ft-lbs"
						MMa=$( echo "scale=3; ($Swe * $ASpeed) /225400" | bc)
						MM="${MMa} ft-lb"
				else
						KE=${KE:="Not posible to calculate without speed data."}
						MM=${MM:="Not posible to calculate without speed data."}
				fi ;;


		*)
				echo "There are missing values to variables so no calculations could be done."
				echo "Set the correct values for one of them."
				sleep 2
				exit ;;

esac
## PRESENT DATA

if [[ $ToFile == "y" ]]; then
	echo "The default file name is Arrowdata.txt. You can set a different name."
	read -p "Name of file (Arrowdata.txt): " FiName
	echo ""
	echo "You can chose to save it as a markdown file instead of a ordinary text file."
	read -p "Do you want a markdown file (y/N)?: " FiTA
	echo ""
	read -p "What is your ARROW NAME (Optional)?: " ANAME
	ANAME=${ANAME:="Unnamed"}
	echo ""
	read -p "Do you want to PRINT THE FILE (y/N)?: " Pri
fi

clear

echo "#######################################################"
echo ""
echo "## THESE ARE THE DATA FOR YOUR ARROWS ##"
echo "-----------------------------------------------------"
echo ""
case $CALC in

		1)
				echo -e "## FOC: ${Bal} % #####"
				echo -e "## BALANCE POINT: ${BalanceP} inches from nock ##### \n" ;;
		2)
				echo -e "## ARROW WEIGHT: ${Swe} Grains #######"
				echo -e "## GRAINS PER POUND: ${Agr} ###### \n" ;;
		3)
				echo -e "## ARROW WEIGHT: ${Swe} Grains #######"
				echo -e "## GRAINS PER POUND: ${Agr} ###### \n"
				echo ""
				echo -e "## KINETIC ENERGY: ${KE} #####"
				echo -e "## MOMENTUM: ${MM} ##### \n" ;;
		4)
				echo -e "## ARROW WEIGHT: ${Swe} Grains #######"
				echo -e "## GRAINS PER POUND: Your ${BowLB} pound bow push ${Agr} Gr/lb with these arrows \n"
						if [[ ! -z $SM ]]; then  #Balance point could theoretically be 0 so 0 would not necessary be an error
								echo ""
								CBP="Balance point was not set, FOC and BALANCE POINT is calculated from weights."
								echo ${CBP}
						else
								echo ""
						fi
				echo -e "## FOC: ${Bal} % #####"
				echo -e "## BALANCE POINT: ${BalanceP} inches from nock ##### \n"
				echo ""
				echo -e "## KINETIC ENERGY: ${KE} #####"
				echo -e "## MOMENTUM: ${MM} ##### \n" ;;
esac

echo "########################################################################"

echo ""
echo ""
CBP=${CBP:=""}
if [[ $ExtraW != 0 ]]; then
		EXW="With ${ExtraW} Gr extra weight in back of inserts"
else
		EXW=""
fi

if [[ $ToFile == "y" || $ToFile == "Y" ]]; then



	FiName=${FiName:="Arrowdata"}

	if [[ $FiTA == "y" || $FiTA == "Y" ]]; then
		FiTyp=md
		cat <<EOT >> "$HOME/${FiName}.md"

--------------------------------------

						ARROW NAME: ${ANAME}

-----------------------------------

ARROW WEIGHT: **${Swe}** Grains

*${EXW}*

GRAINS PER POUND: Your **${BowLB}** lb bow push **${Agr}** Gr/lb
		
*${CBP}*

FOC: **${Bal}** %

BALANCE POINT: **${BalanceP}** inches from nock
				
KINETIC ENERGY: **${KE}**

MOMENTUM: **${MM}**
				
		
EOT
		echo ""

		echo "The results are saved in the file, ${FiName}.md, in your HOME directory"
	else
		FiTyp=txt
		cat <<EOT >> "$HOME/${FiName}.txt"

################################### 
## ARROW NAME: ${ANAME}
################################### 

## ARROW WEIGHT: ${Swe} Grains
--> ${EXW}

## GRAINS PER POUND: Your ${BowLB} pound bow push ${Agr} Gr/lb with these arrows
		
${CBP}
## FOC: ${Bal} %
## BALANCE POINT: ${BalanceP} inches from nock
				
## KINETIC ENERGY: ${KE}
## MOMENTUM: ${MM}
				
		
EOT
		echo ""

		echo "The results are saved in the file, ${FiName}.txt, in your HOME directory"
	fi


			#	echo -e " ## ARROW WEIGHT: ${Swe} Grains \n" "## GRAINS PER POUND: ${Agr} \n" "## FOC: ${Bal} \n" "## BALANCE POINT: ${BalanceP} inches from nock \n" "## KINETIC ENERGY: ${KE} \n" "## MOMENTUM: ${MM} \n\n"
				echo ""
   if [[ $Pri == "y" || $Pri == "Y" ]]; then
      lp "$HOME/${FiName}.${FiTyp}"

      echo "The file have also been printed on your default printer."
   fi

fi
