# Archerytools

Scripts to calculate data of arrows, such as total weight, FOC, and Grains per Pound for your bow.

## BASH Scripts

Add files to e.g. your `$HOME/bin/` folder. Make it executable:

`chmod +x $HOME/bin/ArrowData.sh`

First you will have the choice of calculating the Front Of Center, the arrow weight, Momentum and Kinetic energy or all of them. The script will ask you for the data of your arrow and bow. There are some *default* values in parenthesis and these should be checked and changed according to your own values.


### The parameters to be set are:

**Arrow length:** This is an obligatory value for calculating FOC and is perhaps measured differently by different people. Some measure the whole length of the arrow, that is from the nock tip to the point end. Others measure from the nock groove to the shaft end, which is the AMO standard. Feel free to use whatever length you think is the most correct, but the Arrow length variable is what is used to calculate FOC.

**Shaft length:** When calculating the weight of the arrow it is perhaps not most accurate to include the same length as the one used to calculate FOC. Therefore the shaft length is where you can set the length to be the used for calculating the weight, i.e. to be multiplied with Grains per Inch.

#### Below are values asked if you want the weight of the arrow.

**Grains per Inch:** This value is usually given by the shaft supplier. Without this value you will not get a total weight of the arrow.

**Nr of Vanes:** The number of vanes you use on the arrow. A default of 4 is set.

**Vane weight:** The weight of *each* vane in grains. A default of 2.3 grains is set.

**Nock weight:** The weight of the nock with a default of 12.2 grains.

**Point weight:** The weight of the point with a default of 125 grains. This is only the point, if you use an insert do not take that into account.

**Insert weight:** The weight of the insert, with a default of 12.1 grains. I you do not use inserts, just set it to 0.

**Extra weights:** This is mainly thought of as the extra weight you can screw into the back of the insert of some manufacturers arrows to increase the tip weight without needing a heavier point.

**Draw weight:** The poundage of the weight at your draw length, which can be different from the set weight by the manufacturer of the bow or limbs. The manufacturers usually set the bow weight for a draw of 28 inch and when it comes to recurves, with a riser of 25 inches. If you draw 29 Inches your draw weight will be higher, usually about 2 pound per extra inch of draw length, and vice versa if you draw shorter than 28 inches.
                    
#### The below value is only asked if you want to calculate FOC

**Balance point:** This is the point at which the arrow balance calculated from the nock grove in inches. It is usually found somewhat forward of the center of the arrow. This is what then becomes the forward of center (FOC) of the arrows. How much that should be depends on your preferred arrow behavior, and where and how you shoot. You have a choice to **leave this measure empty** in which case you instead can enter values for the weight of different components of the arrow, as to calculate the weight. The script will then calculate the FOC based on that.

#### The below variable can be set if you want to calculate Kinetic energy and Momentum

**Arrow Speed in foot per second:** This is measured speed of the arrow in feets per second. The Kinetic energy will be presented in feet pounds, ft-lbs. This is probably not a measure of high confidence.

#### The below can be set if you want to save and/or print the results

**Do you want to save the data to a file:** Here you have an option to save your data to a file named "*Arrowdata.txt*" in your `$HOME` directory. The default is *No*.

**The name of the file**: This is an optional name you can set if you want your file to  be called something else than te default "Arrowdata.txt". The script will append every new arrow data to the Arrowdata.txt file or to your specified file **if you set the same name every time** you calculate new data.

**Do you want a markdown file:** This lets you save the data in a markdown file with some small settings. This let you more easy convert it into pdf, docx or html for example. Not sure it is more good looking than the text at this point but you can build on it yourself if you want something more fancy.

**What is the name of the arrow:** You can set this if you want to give the arrows a name. In that way you can separate them in the file "Arrowdata.txt".

**Do you want to print the file:** This gives you the option to also print the file to your default printer. The default is No and it **is at a experimental stage**. I am not sure this will work all the times.
                                    
                    

-----------------------------

### PYTHON Script
This is similar to the bash-script. It asks questions on your arrow and bow ad calculate the data.

